// button bitmask
var LEFT_BUTTON = 1;  // 0001
var RIGHT_BUTTON = 2; // 0010
var REED_SWITCH = 4;  // 0100

SensorTag = {
    count: 0,
	button: {
	    service: "FFE0",
	    data: "FFE1" // Bit 2: side key, Bit 1- right key, Bit 0 –left key
	},

	//http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User%27s_Guide
	accelerometer: {
	    service: "F000AA80-0451-4000-B000-000000000000",
	    data: "F000AA81-0451-4000-B000-000000000000", // read/notify 3 bytes X : Y : Z
	    notification:"F0002902-0451-4000-B000-000000000000",
	    configuration: "F000AA82-0451-4000-B000-000000000000", // read/write 1 byte
	    period: "F000AA83-0451-4000-B000-000000000000" // read/write 1 byte Period = [Input*10]ms
	},

	barometer: {
	    service: "F000AA40-0451-4000-B000-000000000000",
	    data: "F000AA41-0451-4000-B000-000000000000",
	    notification: "F0002902-0451-4000-B000-000000000000",
	    configuration: "F000AA42-0451-4000-B000-000000000000",
	    period: "F000AA43-0451-4000-B000-000000000000"
	},

    ir: {
        service: "F000AA00-0451-4000-B000-000000000000",
        data: "F000AA01-0451-4000-B000-000000000000",
        notification: "F0002902-0451-4000-B000-000000000000",
        configuration: "F000AA02-0451-4000-B000-000000000000",
        period: "F000AA03-0451-4000-B000-000000000000"
    },

    humidity: {
        service: "F000AA20-0451-4000-B000-000000000000",
        data: "F000AA21-0451-4000-B000-000000000000",
        notification: "F0002902-0451-4000-B000-000000000000",
        configuration: "F000AA22-0451-4000-B000-000000000000",
        period: "F000AA23-0451-4000-B000-000000000000"
    },

    optical: {
        service: "F000AA70-0451-4000-B000-000000000000",
        data: "F000AA71-0451-4000-B000-000000000000",
        notification: "F0002902-0451-4000-B000-000000000000",
        configuration: "F000AA72-0451-4000-B000-000000000000",
        period: "F000AA73-0451-4000-B000-000000000000"
    },

    conn: function() {
        ble.scan([], 5,
            function(device){
                Session.set("connected", true);
                Session.set("name", device.name);
                Session.set("id", device.id);
            },
            function(){
                alert('No devices found');
            }
        );
    },
    connect: function(deviceId) {
        onConnect = function() {

        	Session.set("dis", true);
        	Session.set("connected", false);

            //Subscribe to button service
            ble.startNotification(deviceId, SensorTag.button.service, SensorTag.button.data, SensorTag.onButtonData, SensorTag.onError);
            //Subscribe to accelerometer service
            ble.startNotification(deviceId, SensorTag.accelerometer.service, SensorTag.accelerometer.data, SensorTag.onAccelerometerData, SensorTag.onError);
            //Subscribe to barometer service
            ble.startNotification(deviceId, SensorTag.barometer.service, SensorTag.barometer.data, SensorTag.onBarometerData, SensorTag.onError);
            //Subscribe to ir service
            ble.startNotification(deviceId, SensorTag.ir.service, SensorTag.ir.data, SensorTag.onIrData, SensorTag.onError);
            //Subscribe to humidity service
            ble.startNotification(deviceId, SensorTag.humidity.service, SensorTag.humidity.data, SensorTag.onHumidityData, SensorTag.onError);
            //Subscribe to optical service
            ble.startNotification(deviceId, SensorTag.optical.service, SensorTag.optical.data, SensorTag.onOpticalData, SensorTag.onError);

            // turn accelerometer on
            var configData = new Uint16Array(1);
            //Turn on gyro, accel, and mag, 2G range, Disable wake on motion
            configData[0] = 0x007F;
            ble.write(deviceId, SensorTag.accelerometer.service, SensorTag.accelerometer.configuration, configData.buffer,
                function() { console.log("Started accelerometer."); },SensorTag.onError);

            var periodData = new Uint8Array(1);
            periodData[0] = 0x64;
            ble.write(deviceId, SensorTag.accelerometer.service, SensorTag.accelerometer.period, periodData.buffer,
                function() { console.log("Configured accelerometer period."); },SensorTag.onError);

            //Turn on barometer
            var barometerConfig = new Uint8Array(1);
            barometerConfig[0] = 0x01;
            ble.write(deviceId, SensorTag.barometer.service, SensorTag.barometer.configuration, barometerConfig.buffer,
                function() { console.log("Started barometer."); },SensorTag.onError);

            //var periodData = new Uint8Array(1);
            periodData[0] = 0x64;
            //ble.write(deviceId, SensorTag.barometer.service, SensorTag.barometer.period, periodData.buffer,
            //    function() { console.log("Configured barometer period."); },SensorTag.onError);

            //Turn on ir
            var irConfig = new Uint8Array(1);
            irConfig[0] = 0x01;
            ble.write(deviceId, SensorTag.ir.service, SensorTag.ir.configuration, irConfig.buffer,
                function() { console.log("Started ir."); },SensorTag.onError);

            //var periodData = new Uint8Array(1);
            periodData[0] = 0x64;
            ble.write(deviceId, SensorTag.ir.service, SensorTag.ir.period, periodData.buffer,
                function() { console.log("Configured ir period."); },SensorTag.onError);

            //Turn on humidity
            var humidityConfig = new Uint8Array(1);
            humidityConfig[0] = 0x01;
            ble.write(deviceId, SensorTag.humidity.service, SensorTag.humidity.configuration, humidityConfig.buffer,
                function() { console.log("Started humidity."); },SensorTag.onError);

            //var periodData = new Uint8Array(1);
            periodData[0] = 0x64;
            ble.write(deviceId, SensorTag.humidity.service, SensorTag.humidity.period, periodData.buffer,
                function() { console.log("Configured humidity period."); },SensorTag.onError);

            //Turn on optical
            var opticalConfig = new Uint8Array(1);
            opticalConfig[0] = 0x01;
            ble.write(deviceId, SensorTag.optical.service, SensorTag.optical.configuration, opticalConfig.buffer,
                function() { console.log("Started optical."); },SensorTag.onError);

            //var periodData = new Uint8Array(1);
            periodData[0] = 0x64;
            ble.write(deviceId, SensorTag.optical.service, SensorTag.optical.period, periodData.buffer,
                function() { console.log("Configured optical period."); },SensorTag.onError);

            SensorTag.showDetailPage();
        };

        ble.connect(deviceId, onConnect, SensorTag.onError);
    },
    onButtonData: function(data) {
        console.log(data);
        var state = new Uint8Array(data);
        var message = '';

        /*if (state === 0) {
            message = 'No buttons are pressed.';
        }

        if (state & LEFT_BUTTON) {
            message += 'Left button is pressed.<br/>';
        }

        if (state & RIGHT_BUTTON) {
            message += 'Right button is pressed.<br/>';
        }

        if (state & REED_SWITCH) {
            message += 'Reed switch is activated.<br/>';
        }

        var obj = {};
        obj['msg'] = message;

        SensorVal.update({_id: "1"}, { $set: { 
                obj
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_Bs_fun.work(state[0]);
            //alert(p_client.toString());

            var message = new Paho.MQTT.Message(state[0].toString());
            message.destinationName = "sensortag/bs";
            p_client.send(message);
            //p_client.publish(topics.topic_bs, 'hello');
            //data.insert({ topic: topics.topic_bs, message: state[0], broadcast: true });
        };
    },
    sensorMpu9250GyroConvert: function(data){
        return data / (65536/500);
    },

    sensorMpu9250AccConvert: function(data){
        // Change  /2 to match accel 2g range...i.e. 16 g would be /16
        return data / (32768 / 2);
    },

    onAccelerometerData: function(data) {
        console.log(data);
        
        var a = new Int16Array(data);

        /*SensorVal.update({_id: "1"}, { $set: { 
                Gx: SensorTag.sensorMpu9250GyroConvert(a[0]),
                Gy: SensorTag.sensorMpu9250GyroConvert(a[1]),
                Gz: SensorTag.sensorMpu9250GyroConvert(a[2]),
                Ax: SensorTag.sensorMpu9250AccConvert(a[3]),
                Ay: SensorTag.sensorMpu9250AccConvert(a[4]),
                Az: SensorTag.sensorMpu9250AccConvert(a[5]),
                Mx: a[6],
                My: a[7],
                Mz: a[8]
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_Acc_fun.work(SensorTag.sensorMpu9250AccConvert(a[3]), SensorTag.sensorMpu9250AccConvert(a[4]), SensorTag.sensorMpu9250AccConvert(a[5]));
            Chart_Gy_fun.work(SensorTag.sensorMpu9250GyroConvert(a[0]), SensorTag.sensorMpu9250GyroConvert(a[1]), SensorTag.sensorMpu9250GyroConvert(a[2]));
            Chart_Mag_fun.work(a[6], a[7], a[8]);
        };
    },
    sensorBarometerConvert: function(data){
        return (data / 100);
    },
    onBarometerData: function(data) {
         console.log(data);
         
         var a = new Uint8Array(data);

        /*SensorVal.update({_id: "1"}, { $set: { 
                Bt: SensorTag.sensorBarometerConvert( a[0] | (a[1] << 8) | (a[2] << 16)),
                Bp: SensorTag.sensorBarometerConvert( a[3] | (a[4] << 8) | (a[5] << 16))
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_Bar_fun.work(SensorTag.sensorBarometerConvert( a[3] | (a[4] << 8) | (a[5] << 16)));
        };
    },
    sensorTmp007Convert: function(data){
        var SCALE_LSB = 0.03125;
        return ((data >> 2) * SCALE_LSB);

    },
    onIrData: function(data) {
         console.log(data);
         
         var a = new Uint16Array(data);

        /*SensorVal.update({_id: "1"}, { $set: { 
                Iot: SensorTag.sensorTmp007Convert( a[0] ),
                Iat: SensorTag.sensorTmp007Convert( a[1] )
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_T_fun.work(SensorTag.sensorTmp007Convert( a[1] ));
        };
    },
    sensorHdc1000Convert_Temp: function(data){
        return ((data / 65536)*165 - 40);
    },
    sensorHdc1000Convert_Humidity: function(data){
        return ((data / 65536)*100);
    },
    onHumidityData: function(data) {
         console.log(data);
         
         var a = new Uint16Array(data);

        /*SensorVal.update({_id: "1"}, { $set: { 
                T: SensorTag.sensorHdc1000Convert_Temp( a[0] ),
                H: SensorTag.sensorHdc1000Convert_Humidity( a[1] )
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_Hum_fun.work(SensorTag.sensorHdc1000Convert_Humidity( a[1] ));
        };
    },
    sensorOpt3001Convert: function(data){
        var m = data & 0x0FFF;
        var e = (data & 0xF000) >> 12;

        return m * (0.01 * Math.pow(2.0,e));
    },
    onOpticalData: function(data) {
        console.log("optical");
         console.log(data);
         
         var a = new Uint16Array(data);

        /*SensorVal.update({_id: "1"}, { $set: { 
                L: SensorTag.sensorOpt3001Convert( a[0] )
            }
        });*/

        if (SensorTag.count == 0) {
            Chart_Lux_fun.work(SensorTag.sensorOpt3001Convert( a[0] ));
        };
    },
    disconnect: function(deviceId) {
        ble.disconnect(deviceId, SensorTag.showMainPage, SensorTag.onError);
        p_client.disconnect();
        //data.mqttDisconnect();
    },
    showMainPage: function() {
        detailPage.hidden = false;
    },
    showDetailPage: function() {
        detailPage.hidden = false;
    },
    onError: function(reason) {
        alert("ERROR: " + reason);  // real apps should use notification.alert
    }
};