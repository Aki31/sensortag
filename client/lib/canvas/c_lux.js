chart_lux = null;

Data_lux = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  datasets: [
      {
          fillColor: "rgba(0,0,255,0)",
          strokeColor: "rgba(0,0,255,1)",
          pointColor: "rgba(0,0,255,1)",
          pointStrokeColor: "#fff",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
  ]
};

Chart_Lux_fun = {
  work: function(x) {
    if (chart_lux != null) {
      chart_lux.addData([x], ++Data_lux.labels[9]);
      chart_lux.removeData();
    };
  }
};