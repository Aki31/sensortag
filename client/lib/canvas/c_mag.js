chart_mag = null;

Data_mag = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  datasets: [
      {
          fillColor: "rgba(0,0,255,0)",
          strokeColor: "rgba(0,0,255,1)",
          pointColor: "rgba(0,0,255,1)",
          pointStrokeColor: "#fff",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
          fillColor: "rgba(0,255,0,0)",
          strokeColor: "rgba(0,255,0,1)",
          pointColor: "rgba(0,255,0,1)",
          pointStrokeColor: "#fff",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      },
      {
          fillColor: "rgba(255,0,0,0)",
          strokeColor: "rgba(255,0,0,1)",
          pointColor: "rgba(255,0,0,1)",
          pointStrokeColor: "#fff",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
  ]
};

Chart_Mag_fun = {
  work: function(x, y, z) {
    if (chart_mag != null) {
      chart_mag.addData([x, y, z], ++Data_mag.labels[9]);
      chart_mag.removeData();
    };
  }
};