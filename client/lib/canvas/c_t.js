chart_t = null;

Data_t = {
  labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  datasets: [
      {
          fillColor: "rgba(0,0,255,0)",
          strokeColor: "rgba(0,0,255,1)",
          pointColor: "rgba(0,0,255,1)",
          pointStrokeColor: "#fff",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }
  ]
};

Chart_T_fun = {
  work: function(x) {
    if (chart_t != null) {
      chart_t.addData([x], ++Data_t.labels[9]);
      chart_t.removeData();
    };
  }
};