data_mqtt = new Mongo.Collection("ascoltatori");

if (Meteor.isCordova) {
    Meteor.startup(function () {
    	document.addEventListener("backbutton", function(){
			if (history.state && history.state.initial === true) {
                SensorTag.disconnect();
				navigator.app.exitApp();
			} else {
                history.go(-1);
            }
		});

        Session.set("mobile", "true");

    	Session.set("name", "unknown");
    	Session.set("id", 0);
    	Session.set("connected", false);
    	Session.set("dis", false);
    	SensorTag.conn();

        //var mqtt_m = require('mqtt');

        p_client = new Paho.MQTT.Client("oizomiotdev.cloudapp.net", 1884, "Akshit_App");//('tcp://192.168.1.14:1883', mqtt_settings);
        p_client.onConnectionLost = function (responseObject) {
            console.log("connection lost: " + responseObject.errorMessage);
        };
        p_client.onMessageArrived = function (message) {
            console.log(message.destinationName, ' -- ', message.payloadString);
        };

        var options = {
            userName: "akshit",
            password: "akshit1994",
            onSuccess: function () {
                console.log("mqtt connected");

                //use the below if you want to publish to a topic on connect
                message = new Paho.MQTT.Message("Hello");
                message.destinationName = "sensortag/gen";
                p_client.send(message);
            },
            onFailure: function (message) {
                console.log("Connection failed: " + message.errorMessage);
            }
        };

        p_client.connect(options);

    });
}

if ( ! Meteor.isCordova) {
    Meteor.startup(function () {
        //t_client = mqtt.connect('mqtt://192.168.0.107:1883');

        //t_client.subscribe(topic_gen);

        Meteor.subscribe("data");

        Session.set("mobile", "false");

        Session.set("connected", false);
        Session.set("dis", false);
    });
}