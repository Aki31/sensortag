Template.data.helpers({
	name: function() {
		return Session.get("name");
	},
	id: function() {
		return Session.get("id");
	},
	dis: function() {
		return Session.get("dis");
	}
});