Template.details.rendered = function() {
	var ctx_acc = this.find('#acc_c').getContext('2d');
	chart_acc = new Chart(ctx_acc).Line(Data_acc, {animation: false});

	var ctx_bar = this.find('#bar_c').getContext('2d');
	chart_bar = new Chart(ctx_bar).Line(Data_bar, {animation: false});

	var ctx_bs = this.find('#bs_c').getContext('2d');
	chart_bs = new Chart(ctx_bs).Line(Data_bs, {animation: false});

	var ctx_gy = this.find('#gy_c').getContext('2d');
	chart_gy = new Chart(ctx_gy).Line(Data_gy, {animation: false});

	var ctx_hum = this.find('#hum_c').getContext('2d');
	chart_hum = new Chart(ctx_hum).Line(Data_hum, {animation: false});

	var ctx_lux = this.find('#opt_c').getContext('2d');
	chart_lux = new Chart(ctx_lux).Line(Data_lux, {animation: false});

	var ctx_mag = this.find('#mag_c').getContext('2d');
	chart_mag = new Chart(ctx_mag).Line(Data_mag, {animation: false});

	var ctx_t = this.find('#ir_c').getContext('2d');
	chart_t = new Chart(ctx_t).Line(Data_t, {animation: false});
};