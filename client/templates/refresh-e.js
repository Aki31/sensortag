Template.refresh.events({
	"click .scan": function() {
		if (Meteor.isCordova) {
			if (Session.get("dis")) {
				var id = Session.get("id");
				Session.set("dis", false);
				Session.set("connected", false);
                Session.set("name", "unknown");
                Session.set("id", 0);
                SensorTag.disconnect(id);
			} else {
				SensorTag.conn();
			}
		}
	}
});