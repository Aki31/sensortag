Template.topics.helpers({
	data: function() {
		//console.log(data_mqtt);
		return data_mqtt.find({ topic: { $regex: "^sensortag" } }).fetch();
	},
	fields: function() {
		return [
				{ key: 'topic', label: 'Topic', cellClass: 'col-md-4' },
				{ key: 'value', label: 'Value', cellClass: 'col-md-4', sortable: false, fn: function(value, object, key) { 
						var arr = "";
						for(var i = 0; i < value.length; i++){
							arr += String.fromCharCode(value[i]);
						}
						return arr; 
					} 
				},
				{ key: 'options.clientId', label: 'Client', cellClass: 'col-md-4' }
			];
	},
	settings: function() {
		return {};
	}
});