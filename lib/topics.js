topics = {
	topic_all: 'sensortag/#',
	topic_gen: 'sensortag/gen',	//anything else
	topic_acc: 'sensortag/acc',
	topic_bar: 'sensortag/bar',
	topic_bs: 'sensortag/bs',
	topic_gy: 'sensortag/gy',
	topic_hum: 'sensortag/hum',
	topic_lux: 'sensortag/lux',
	topic_mag: 'sensortag/mag',
	topic_t: 'sensortag/t'
};