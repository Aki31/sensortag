// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
  id: 'aki.sensortag',
  name: 'SensorTag',
  description: 'Display SensorTag data',
  author: 'Akshit Bhandari',
  email: 'bantu1972@gmail.com',
  website: 'http://jisfora.com',
  version: "0.0.1"
});

// Set up resources such as icons and launch screens.
App.icons({
  'iphone': 'public/icons/icon.png',
  'android_ldpi': 'public/icons/icon.png',
  'android_hdpi': 'public/icons/icon.png',
  'android_xhdpi': 'public/icons/icon.png',
  'android_mdpi': 'public/icons/icon.png',
});

App.launchScreens({
  'iphone': 'public/icons/splash.png',
  'android_ldpi_portrait': 'public/icons/splash.png',
  'android_hdpi_portrait': 'public/icons/splash.png',
  'android_xhdpi_portrait': 'public/icons/splash.png',
  'android_mdpi_portrait': 'public/icons/splash.png',
});

// Set PhoneGap/Cordova preferences
App.setPreference('BackgroundColor', '0x31244eff');
App.setPreference('HideKeyboardFormAccessoryBar', true);