Mosca_Set = {
	mosca: null,
	pubsubsettings: {
			//using ascoltatore
			type: 'mongo',
			url: 'mongodb://localhost:27017/meteor',
			pubsubCollection: 'ascoltatori',
			mongo: {}
		},
	run: function(){
		var settings = {
		//host: '192.168.1.14',
		port: 5000,
		backend: Mosca_Set.pubsubsettings,
		persistence: Mosca_Set.mosca.persistence.Mongo,
		http: {
		    port: 5001,
		    bundle: true,
		    static: './'
		}
	};

	var server = new Mosca_Set.mosca.Server(settings,function(){

	});
	server.on('ready', function() {
	  console.log('Mosca server is up and running');
	  server.authenticate = authenticate;
	  server.authorizePublish = authorizePublish;
	  server.authorizeSubscribe = authorizeSubscribe;
	}); 

	server.on('published', function(packet, client) {

		if (packet.topic.indexOf('akshit') === 0) {
			return;
		}

		console.log('Published', packet);
		//console.log('Client', client);

		/*var newPacket = {
			topic: 'akshit/' + packet.topic,
			payload: packet.payload,
			retain: packet.retain,
			qos: packet.qos
		};*/

		packet.topic = 'akshit/' + packet.topic;

		server.publish(packet, function(){
			console.log('done!');
		});
	});

	// fired when a client connects
	server.on('clientConnected', function(client) {
		console.log('Client Connected:', client.id);
	});

	// fired when a client disconnects
	server.on('clientDisconnected', function(client) {
		console.log('Client Disconnected:', client.id);
	});

	// Accepts the connection if the username and password are valid
	var authenticate = function(client, username, password, callback) {
	  var authorized = (username === 'akshit' && password.toString() === 'akshit1994');
	  if (authorized) client.user = username;
	  callback(null, authorized);
	}

	// In this case the client authorized as alice can publish to /users/alice taking
	// the username from the topic and verifing it is the same of the authorized user
	var authorizePublish = function(client, topic, payload, callback) {
	  callback(null, client.user == 'akshit');
	}

	// In this case the client authorized as alice can subscribe to /users/alice taking
	// the username from the topic and verifing it is the same of the authorized user
	var authorizeSubscribe = function(client, topic, callback) {
		console.log(client.user);
	  callback(null, client.user == 'akshit');
	}
	}
};